﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProyectoProgramacion.Models;

namespace ProyectoProgramacion.Controllers
{
    public class NotasController : Controller
    {
        private proyectoContainer db = new proyectoContainer();

        // GET: /Notas/
        public ActionResult Index()
        {
            return View(db.NotasSet.ToList());
        }

        // GET: /Notas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Notas notas = db.NotasSet.Find(id);
            if (notas == null)
            {
                return HttpNotFound();
            }
            return View(notas);
        }

        // GET: /Notas/Create
        public ActionResult Create()
        {
            ViewBag.grados = new SelectList(db.GradoSet.ToList(), "Id_Grado", "Nombre_Grado");
            ViewBag.asignaturas = new SelectList(db.AsignaturaSet.ToList(), "Id_Asignatura", "Nombre_Asig");
            ViewBag.estudiantes = new SelectList(db.EstudianteSet.ToList(), "Id_Estudiante", "Nombre_Estu");
            //return View(new CargaAcademica { Id_Docente = Id });
            return View();
            
        }

        // POST: /Notas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id_Notas,valor,Id_Asignatura,Id_Estudiante,Id_Grado")] Notas notas)
        {
            if (ModelState.IsValid)
            {
                db.NotasSet.Add(notas);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(notas);
        }

        // GET: /Notas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Notas notas = db.NotasSet.Find(id);
            if (notas == null)
            {
                return HttpNotFound();
            }
            return View(notas);
        }

        // POST: /Notas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id_Notas,valor,Id_Asignatura,Id_Estudiante")] Notas notas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(notas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(notas);
        }

        // GET: /Notas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Notas notas = db.NotasSet.Find(id);
            if (notas == null)
            {
                return HttpNotFound();
            }
            return View(notas);
        }

        // POST: /Notas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Notas notas = db.NotasSet.Find(id);
            db.NotasSet.Remove(notas);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
