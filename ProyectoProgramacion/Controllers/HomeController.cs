﻿using ProyectoProgramacion.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoProgramacion.Controllers
{
    public class HomeController : Controller
    {
        private proyectoContainer db = new proyectoContainer();
        public ActionResult Index()
        {
            ViewBag.Message = db.ActividadSet.Where(x => x.Nombre_Pagina == "Principal").ToList();
            return View();
        }

        public ActionResult Personitas()
        {
            ViewBag.Message = db.ActividadSet.Where(x=>x.Nombre_Pagina == "personitas").ToList();
            return View();
        }

        public ActionResult Administracion()
        {
            ViewBag.Message = db.ActividadSet.Where(x => x.Nombre_Pagina == "Administracion").ToList();            
            return View();
        }

        public ActionResult Search()
        {
            return View(new Estudiante());
        }

        [HttpPost]
        public ActionResult Search(Estudiante model)
        {
            var est = db.EstudianteSet.SingleOrDefault(x => x.Nombre_Estu == model.Nombre_Estu);
            return RedirectToAction("notas", new { id = est.Id_Estudiante });
        }

        //public ActionResult Buscar()
        //{
        //    return View();
        //}

        
        //[HttpPost]
        //public ActionResult Buscar(Estudiante model)
        //{
            
        //}

        public ActionResult fami()
        {

            ViewBag.Message = db.ActividadSet.Where(x => x.Nombre_Pagina == "familia").ToList();            
            return View();
        }

        public ActionResult San()
        {
            ViewBag.Message = db.ActividadSet.Where(x => x.Nombre_Pagina == "sanpedro").ToList();            
            return View();
        }


        public ActionResult niño()
        {

            ViewBag.Message = db.ActividadSet.Where(x => x.Nombre_Pagina == "nino").ToList();            
            return View();
        }


        public ActionResult hallo()
        {
            ViewBag.Message = db.ActividadSet.Where(x => x.Nombre_Pagina == "amoramistad").ToList();            

            return View();
        }


        public ActionResult notas(int id)
        {
            ViewBag.Estudiante = db.EstudianteSet.SingleOrDefault(x => x.Id_Estudiante == id);
            int g =  (int)ViewBag.Estudiante.Id_Grado;
            ViewBag.Grado = db.GradoSet.SingleOrDefault(x => x.Id_Grado == g);
            var notasModel = db.NotasSet
                .Where(x => x.Id_Estudiante == id)
                .Select(x => new NotaExt
                {
                    valor = x.valor,
                    Nombre_Asig = db.AsignaturaSet.FirstOrDefault(a => a.Id_Asignatura == x.Id_Asignatura).Nombre_Asig,
                    Id_Asignatura =  x.Id_Asignatura
                })
                .ToList();
            return View(notasModel);
        }
    }

    public class NotaExt : Notas
    {
        public string Nombre_Asig { get; set; }
    }
}