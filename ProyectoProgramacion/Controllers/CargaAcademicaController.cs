﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProyectoProgramacion.Models;

namespace ProyectoProgramacion.Controllers
{
    public class CargaAcademicaController : Controller
    {
        private proyectoContainer db = new proyectoContainer();

        // GET: /CargaAcademica/
        public ActionResult Index()
        {
            return View(db.CargaAcademicaSet.ToList());
        }

        // GET: /CargaAcademica/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CargaAcademica cargaacademica = db.CargaAcademicaSet.Find(id);
            if (cargaacademica == null)
            {
                return HttpNotFound();
            }
            return View(cargaacademica);
        }

        // GET: /CargaAcademica/Create
        public ActionResult Create(int Id)
        {
            ViewBag.grados = new SelectList(db.GradoSet.ToList(), "Id_Grado", "Nombre_Grado");
            ViewBag.asignaturas = new SelectList(db.AsignaturaSet.ToList(), "Id_Asignatura", "Nombre_Asig");
            return View(new CargaAcademica { Id_Docente=Id});
        }

        // POST: /CargaAcademica/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Id_Grado,Id_Docente,Id_Asignatura")] CargaAcademica cargaacademica)
        {
            if (ModelState.IsValid)
            {
                db.CargaAcademicaSet.Add(cargaacademica);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cargaacademica);
        }

        // GET: /CargaAcademica/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CargaAcademica cargaacademica = db.CargaAcademicaSet.Find(id);
            if (cargaacademica == null)
            {
                return HttpNotFound();
            }
            return View(cargaacademica);
        }

        // POST: /CargaAcademica/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Id_Grado,Id_Docente,Id_Asinatura")] CargaAcademica cargaacademica)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cargaacademica).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cargaacademica);
        }

        // GET: /CargaAcademica/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CargaAcademica cargaacademica = db.CargaAcademicaSet.Find(id);
            if (cargaacademica == null)
            {
                return HttpNotFound();
            }
            return View(cargaacademica);
        }

        // POST: /CargaAcademica/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CargaAcademica cargaacademica = db.CargaAcademicaSet.Find(id);
            db.CargaAcademicaSet.Remove(cargaacademica);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
