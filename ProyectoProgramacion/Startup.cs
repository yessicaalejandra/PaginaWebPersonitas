﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ProyectoProgramacion.Startup))]
namespace ProyectoProgramacion
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
