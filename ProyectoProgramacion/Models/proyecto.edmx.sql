
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 08/01/2015 01:35:48
-- Generated from EDMX file: C:\Users\USUARIO\Desktop\ProyectoProgramacion\ProyectoProgramacion\Models\proyecto.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Centro_Personitas];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[ActividadSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ActividadSet];
GO
IF OBJECT_ID(N'[dbo].[NotasSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[NotasSet];
GO
IF OBJECT_ID(N'[dbo].[RolSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[RolSet];
GO
IF OBJECT_ID(N'[dbo].[AsignaturaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AsignaturaSet];
GO
IF OBJECT_ID(N'[dbo].[DocenteSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DocenteSet];
GO
IF OBJECT_ID(N'[dbo].[EstudianteSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[EstudianteSet];
GO
IF OBJECT_ID(N'[dbo].[GradoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GradoSet];
GO
IF OBJECT_ID(N'[dbo].[CargaAcademicaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CargaAcademicaSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'ActividadSet'
CREATE TABLE [dbo].[ActividadSet] (
    [Id_Actividad] int IDENTITY(1,1) NOT NULL,
    [Titulo_Actividad] nvarchar(max)  NOT NULL,
    [Des_Actividad] nvarchar(max)  NOT NULL,
    [Nombre_Pagina] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'NotasSet'
CREATE TABLE [dbo].[NotasSet] (
    [Id_Notas] int IDENTITY(1,1) NOT NULL,
    [valor] nvarchar(max)  NOT NULL,
    [Id_Asignatura] int  NOT NULL,
    [Id_Estudiante] int  NOT NULL,
    [Id_Grado] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'RolSet'
CREATE TABLE [dbo].[RolSet] (
    [Id_Rol] int IDENTITY(1,1) NOT NULL,
    [tipo_rol] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'AsignaturaSet'
CREATE TABLE [dbo].[AsignaturaSet] (
    [Id_Asignatura] int IDENTITY(1,1) NOT NULL,
    [Nombre_Asig] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'DocenteSet'
CREATE TABLE [dbo].[DocenteSet] (
    [Id_Docente] int IDENTITY(1,1) NOT NULL,
    [Nombre_Docente] nvarchar(max)  NOT NULL,
    [Apellido_Docente] nvarchar(max)  NOT NULL,
    [Tipo_Doc] nvarchar(max)  NOT NULL,
    [Numero_Doc] int  NOT NULL,
    [Id_Rol] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'EstudianteSet'
CREATE TABLE [dbo].[EstudianteSet] (
    [Id_Estudiante] int IDENTITY(1,1) NOT NULL,
    [Id_Grado] int  NOT NULL,
    [Nombre_Estu] nvarchar(max)  NOT NULL,
    [Apellido_Estu] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'GradoSet'
CREATE TABLE [dbo].[GradoSet] (
    [Id_Grado] int IDENTITY(1,1) NOT NULL,
    [Nombre_Grado] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'CargaAcademicaSet'
CREATE TABLE [dbo].[CargaAcademicaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Id_Grado] int  NOT NULL,
    [Id_Docente] int  NOT NULL,
    [Id_Asignatura] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id_Actividad] in table 'ActividadSet'
ALTER TABLE [dbo].[ActividadSet]
ADD CONSTRAINT [PK_ActividadSet]
    PRIMARY KEY CLUSTERED ([Id_Actividad] ASC);
GO

-- Creating primary key on [Id_Notas] in table 'NotasSet'
ALTER TABLE [dbo].[NotasSet]
ADD CONSTRAINT [PK_NotasSet]
    PRIMARY KEY CLUSTERED ([Id_Notas] ASC);
GO

-- Creating primary key on [Id_Rol] in table 'RolSet'
ALTER TABLE [dbo].[RolSet]
ADD CONSTRAINT [PK_RolSet]
    PRIMARY KEY CLUSTERED ([Id_Rol] ASC);
GO

-- Creating primary key on [Id_Asignatura] in table 'AsignaturaSet'
ALTER TABLE [dbo].[AsignaturaSet]
ADD CONSTRAINT [PK_AsignaturaSet]
    PRIMARY KEY CLUSTERED ([Id_Asignatura] ASC);
GO

-- Creating primary key on [Id_Docente] in table 'DocenteSet'
ALTER TABLE [dbo].[DocenteSet]
ADD CONSTRAINT [PK_DocenteSet]
    PRIMARY KEY CLUSTERED ([Id_Docente] ASC);
GO

-- Creating primary key on [Id_Estudiante] in table 'EstudianteSet'
ALTER TABLE [dbo].[EstudianteSet]
ADD CONSTRAINT [PK_EstudianteSet]
    PRIMARY KEY CLUSTERED ([Id_Estudiante] ASC);
GO

-- Creating primary key on [Id_Grado] in table 'GradoSet'
ALTER TABLE [dbo].[GradoSet]
ADD CONSTRAINT [PK_GradoSet]
    PRIMARY KEY CLUSTERED ([Id_Grado] ASC);
GO

-- Creating primary key on [Id] in table 'CargaAcademicaSet'
ALTER TABLE [dbo].[CargaAcademicaSet]
ADD CONSTRAINT [PK_CargaAcademicaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------